//
//  main.m
//  TabBarApplication
//
//  Created by Adil Hussain on 09/09/2016.
//  Copyright © 2016 Masabi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
