//
//  Photographer+Create.h
//  StanfordLectures
//
//  Created by Adil Hussain on 06/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Photographer+CoreDataProperties.h"

#define PHOTOGRAPHER_NAME @"name"

@interface Photographer (Create)

+ (Photographer *)insertPhotographerWithData:(NSDictionary *)photographerData
                    intoManagedObjectContext:(NSManagedObjectContext *)context;

@end
