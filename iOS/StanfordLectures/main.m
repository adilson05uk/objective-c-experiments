//
//  main.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
