//
//  ViewA.m
//  StanfordLectures
//
//  Created by Adil Hussain on 22/08/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewA.h"

@implementation ViewA

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (void)setup {
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (void)drawRect:(CGRect)rect {
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:10.0];
    [roundedRect addClip];
    
    [self.fillColor setFill];
    UIRectFill(self.bounds);
    
    [self.strokeColor setFill];
    [roundedRect stroke];
    
    [self drawImage];
    [self drawText];
}

- (void)setFillColor:(UIColor *)fillColor {
    _fillColor = fillColor;
    [self setNeedsDisplay];
}

- (void)setStrokeColor:(UIColor *)strokeColor {
    _strokeColor = strokeColor;
    [self setNeedsDisplay];
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    [self setNeedsDisplay];
}

- (void)drawImage {
    UIImage *image = [UIImage imageNamed:@"Clock"];
    
    CGFloat width = self.bounds.size.width * 0.25;
    CGFloat height = self.bounds.size.height * 0.25;
    
    CGRect imageRect = CGRectInset(self.bounds, width, height);
    
    [image drawInRect:imageRect];
}

- (void)drawText {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    UIFont *font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    
    NSAttributedString *text = [[NSAttributedString alloc] initWithString:@"This is text!"
                                                               attributes:@{ NSFontAttributeName : font,
                                                                             NSParagraphStyleAttributeName : paragraphStyle,
                                                                             NSForegroundColorAttributeName : self.textColor }];
    
    CGRect textBounds;
    textBounds.origin = CGPointMake(5.0, 5.0);
    textBounds.size = [text size];
    [text drawInRect:textBounds];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, self.bounds.size.width, self.bounds.size.height);
    CGContextRotateCTM(context, M_PI);
    [text drawInRect:textBounds];
}

@end
