//
//  TableViewCellA.h
//  StanfordLectures
//
//  Created by Adil Hussain on 25/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellA : UITableViewCell

+ (NSString *)reuseIdentifier;
+ (NSString *)nibName;

- (void)configureViewsForRow:(NSInteger)row;

@end
