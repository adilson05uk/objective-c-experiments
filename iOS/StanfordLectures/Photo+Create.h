//
//  Photo+Create.h
//  StanfordLectures
//
//  Created by Adil Hussain on 16/12/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Photo+CoreDataProperties.h"

#define PHOTO_DESCRIPTION @"description"
#define PHOTO_TITLE @"title"

@interface Photo (Create)

+ (Photo *)insertPhotoWithData:(NSDictionary *)photoData
                  photographer:(Photographer *)photographer
      intoManagedObjectContext:(NSManagedObjectContext *)context;

@end
