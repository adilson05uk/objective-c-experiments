//
//  ViewA.h
//  StanfordLectures
//
//  Created by Adil Hussain on 22/08/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewA : UIView

@property (nonatomic) UIColor *fillColor;
@property (nonatomic) UIColor *strokeColor;
@property (nonatomic) UIColor *textColor;

@end
