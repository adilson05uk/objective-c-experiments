//
//  ViewControllerB.m
//  StanfordLectures
//
//  Created by Adil Hussain on 21/06/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerB.h"

@interface ViewControllerB ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation ViewControllerB

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller B"];
}

- (IBAction)buttonPressed:(id)sender {
    NSUInteger index = [self.buttons indexOfObject:sender];
    NSString *message = [NSString stringWithFormat:@"Button at index = %lu", index];
    
    [self showAlertWithTitle:@"Button pressed" message:message];
}

@end
