//
//  Photographer+Create.m
//  StanfordLectures
//
//  Created by Adil Hussain on 06/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "Photographer+Create.h"

@implementation Photographer (Create)

+ (Photographer *)insertPhotographerWithData:(NSDictionary *)photographerData
                    intoManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [Photographer fetchRequest];
    
    Photographer *photographer = [NSEntityDescription insertNewObjectForEntityForName:request.entityName
                                                               inManagedObjectContext:context];
    
    photographer.name = photographerData[PHOTOGRAPHER_NAME];
    photographer.removed = NO;
    
    [context save:NULL];
    
    return photographer;
}

@end
