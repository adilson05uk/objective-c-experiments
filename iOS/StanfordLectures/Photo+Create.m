//
//  Photo+Create.m
//  StanfordLectures
//
//  Created by Adil Hussain on 16/12/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "Photo+Create.h"
#import "Photographer+Create.h"

@implementation Photo (Create)

+ (Photo *)insertPhotoWithData:(NSDictionary *)photoData
                  photographer:(Photographer *)photographer
      intoManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [Photo fetchRequest];
    
    Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:request.entityName
                                                 inManagedObjectContext:context];
    
    photo.removed = NO;
    photo.subtitle = photoData[PHOTO_DESCRIPTION];
    photo.title = photoData[PHOTO_TITLE];
    photo.whoTook = photographer;
    
    [context save:NULL];
    
    return photo;
}

@end
