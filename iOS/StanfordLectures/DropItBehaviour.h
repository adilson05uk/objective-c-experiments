//
//  DropItBehaviour.h
//  StanfordLectures
//
//  Created by Adil Hussain on 31/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropItBehaviour : UIDynamicBehavior

- (void)addItem:(id <UIDynamicItem>)item;
- (void)removeItem:(id <UIDynamicItem>)item;

@end
