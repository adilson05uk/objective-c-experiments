//
//  ViewControllerK.h
//  StanfordLectures
//
//  Created by Adil Hussain on 13/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "CoreDataTableViewController.h"
#import "Photographer+CoreDataProperties.h"

@interface ViewControllerK : CoreDataTableViewController

@property (nonatomic, strong) Photographer *photographer;

@end
