//
//  ProtocolA.h
//  StanfordLectures
//
//  Created by Adil Hussain on 27/09/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProtocolA <NSObject>

@required

-(int)matchAgainstSingleItem:(NSString *)item;
-(int)matchAgainstMultipleItems:(NSArray *)items;

@optional

-(void)someOptionalMethod;

@end
