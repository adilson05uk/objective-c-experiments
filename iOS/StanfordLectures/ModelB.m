//
//  ModelB.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ModelB.h"

@interface ModelB()

@property (strong, nonatomic) NSMutableArray *propertyMutableArray;

@end

@implementation ModelB

-(instancetype)init {
    if (self = [super init]) {
        self.propertyMutableArray = [NSMutableArray new];
    }
    
    return self;
}

-(void)addItem:(NSString *)item {
    [self.propertyMutableArray addObject:item];
}

-(NSString *)removeItem {
    NSUInteger count = [self.propertyMutableArray count];
    
    if (count == 0) {
        return nil;
    }
    
    NSUInteger index = count - 1;
    id item = self.propertyMutableArray[index];
    [self.propertyMutableArray removeObjectAtIndex:index];
    return item;
}

-(NSString *)description {
    NSString *className = NSStringFromClass([self class]);
    return [NSString stringWithFormat:@"%@ - Model class that has a private property.", className];
}

@end
