//
//  DropItBehaviour.m
//  StanfordLectures
//
//  Created by Adil Hussain on 31/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "DropItBehaviour.h"

@implementation DropItBehaviour {
    UIGravityBehavior *_gravityBehaviour;
    UICollisionBehavior *_collisionBehaviour;
    UIDynamicItemBehavior *_itemBehaviour;
}

- (instancetype)init {
    self = [super init];
    
    _gravityBehaviour = [[UIGravityBehavior alloc] init];
    _gravityBehaviour.magnitude = 1.0;
    
    _collisionBehaviour = [[UICollisionBehavior alloc] init];
    _collisionBehaviour.translatesReferenceBoundsIntoBoundary = YES;
    
    _itemBehaviour = [[UIDynamicItemBehavior alloc] init];
    _itemBehaviour.allowsRotation = NO;
    
    [self addChildBehavior:_gravityBehaviour];
    [self addChildBehavior:_collisionBehaviour];
    [self addChildBehavior:_itemBehaviour];

    return self;
}

- (void)addItem:(id <UIDynamicItem>)item {
    [_gravityBehaviour addItem:item];
    [_collisionBehaviour addItem:item];
    [_itemBehaviour addItem:item];
}

- (void)removeItem:(id <UIDynamicItem>)item {
    [_gravityBehaviour removeItem:item];
    [_collisionBehaviour removeItem:item];
    [_itemBehaviour removeItem:item];
}

@end
