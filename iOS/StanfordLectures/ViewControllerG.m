//
//  ViewControllerG.m
//  StanfordLectures
//
//  Created by Adil Hussain on 31/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerG.h"
#import "BezierPathView.h"
#import "DropItBehaviour.h"

@interface ViewControllerG () <UIDynamicAnimatorDelegate>
@end

@implementation ViewControllerG {
    __weak IBOutlet BezierPathView *_gameView;
    UIView *_dropView;
    
    UIDynamicAnimator *_animator;
    DropItBehaviour *_dropItBehaviour;
    UIAttachmentBehavior *_attachmentBehaviour;
}

static const CGSize DROP_SIZE = { 40, 40 };

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller G"];
    
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:_gameView];
    _dropItBehaviour = [[DropItBehaviour alloc] init];
    
    [_animator addBehavior:_dropItBehaviour];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self drop];
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    CGPoint gesturePoint = [sender locationInView:_gameView];
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            NSLog(@"Pan gesture started");
            [self attachDropViewToPoint:gesturePoint];
            break;
        case UIGestureRecognizerStateChanged:
            _attachmentBehaviour.anchorPoint = gesturePoint;
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"Pan gesture ended");
            [self removeAttachmentBehaviour];
            break;
        default:
            break;
    }
}
 
- (void)drop {
    int x = (arc4random() % (int)_gameView.bounds.size.width) / DROP_SIZE.width;
    
    CGRect frame;
    frame.origin = CGPointZero;
    frame.origin.x = x * DROP_SIZE.width;
    frame.size = DROP_SIZE;
    
    _dropView = [[UIView alloc] initWithFrame:frame];
    _dropView.backgroundColor = [self randomColor];
    
    [_gameView addSubview:_dropView];
    
    [_dropItBehaviour addItem:_dropView];
    
    __weak UIView *weakDropView = _dropView;
    
    [self performSelector:@selector(animateRemovingDropView:)
               withObject:weakDropView
               afterDelay:5.0];
}

- (void)attachDropViewToPoint:(CGPoint)anchorPoint {
    if (_dropView) {
        _attachmentBehaviour = [[UIAttachmentBehavior alloc] initWithItem:_dropView
                                                         attachedToAnchor:anchorPoint];
        
        __weak UIAttachmentBehavior *weakAttachmentBehaviour = _attachmentBehaviour;
        __weak BezierPathView *weakGameView = _gameView;
        __weak UIView *weakDropView = _dropView;
        
        _attachmentBehaviour.action = ^{
            UIBezierPath *path = [[UIBezierPath alloc] init];
            [path moveToPoint:weakAttachmentBehaviour.anchorPoint];
            [path addLineToPoint:weakDropView.center];
            
            weakGameView.path = path;
        };
        
        [_animator addBehavior:_attachmentBehaviour];
    }
}

- (void)animateRemovingDropView:(UIView *)dropView {
    NSLog(@"Start remove drop view animation");
    
    if ([dropView isEqual:_dropView]) {
        _dropView = nil;
    }
    
    [_dropItBehaviour removeItem:dropView];
    [self removeAttachmentBehaviour];
    
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         float x = self->_gameView.bounds.size.width * 1.5;
                         float y = self->_gameView.bounds.size.height / 2.0;
                         dropView.center = CGPointMake(x, y);
                     }
                     completion:^(BOOL finished){
                         [dropView removeFromSuperview];
                     }];
}

- (void)removeAttachmentBehaviour {
    [_animator removeBehavior:_attachmentBehaviour];
    _attachmentBehaviour = nil;
    _gameView.path = nil;
}

- (UIColor *)randomColor {
    switch (arc4random() % 5) {
        case 0: return [UIColor greenColor];
        case 1: return [UIColor blueColor];
        case 2: return [UIColor orangeColor];
        case 3: return [UIColor redColor];
        default: return [UIColor purpleColor];
    }
}

@end
