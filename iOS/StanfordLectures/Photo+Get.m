//
//  Photo+Get.m
//  StanfordLectures
//
//  Created by Adil Hussain on 20/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "Photo+Get.h"

@implementation Photo (Get)

+ (Photo *)getPhotoWithObjectID:(NSManagedObjectID *)objectID
       fromManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [Photo fetchRequest];
    request.predicate = [NSPredicate predicateWithFormat:@"self = %@", objectID];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    Photo *photo = nil;
    
    if (error || !results || results.count > 1) {
        NSLog(@"Error fetching Photo objects: %@\n%@", [error localizedDescription], [error userInfo]);
    } else {
        photo = results.lastObject;
    }
    
    return photo;
}

@end
