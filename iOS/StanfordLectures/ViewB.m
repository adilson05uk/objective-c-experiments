//
//  ViewB.m
//  StanfordLectures
//
//  Created by Adil Hussain on 23/09/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewB.h"

@implementation ViewB

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (void)setup {
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (void)drawRect:(CGRect)rect {
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path setLineWidth:2.0];
    [path moveToPoint:CGPointMake(rect.size.width / 2, 0)];
    [path addLineToPoint:CGPointMake(rect.size.width, rect.size.height)];
    [path addLineToPoint:CGPointMake(0, rect.size.height)];
    [path closePath];
    
    [[UIColor greenColor] setFill];
    [[UIColor redColor] setStroke];
    
    [path fill];
    [path stroke];
}

@end
