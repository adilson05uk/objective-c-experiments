//
//  ViewControllerH.m
//  StanfordLectures
//
//  Created by Adil Hussain on 18/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerH.h"

@interface ViewControllerH () <UIScrollViewDelegate>
@end

@implementation ViewControllerH {
    NSArray *_imageURLStrings;
    UIImageView *_imageView;
    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet UIActivityIndicatorView *_loadingIndicator;
    NSInteger _currentIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller H"];
    
    UIBarButtonItem *previousButton = [[UIBarButtonItem alloc] initWithTitle:@"Previous"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(previousImage)];
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(nextImage)];
    
    self.navigationItem.rightBarButtonItems = @[nextButton, previousButton];
    
    _imageURLStrings = @[@"https://c1.staticflickr.com/9/8545/28823229733_43ed0ebcb6_b.jpg",
                        @"https://c1.staticflickr.com/9/8034/29156603560_435be8c2a2_b.jpg",
                        @"https://c1.staticflickr.com/9/8453/29156601040_751e2da53e_b.jpg"];
    
    _currentIndex = 0;
    
    _imageView = [[UIImageView alloc] init];
    
    [_scrollView setMinimumZoomScale:0.2];
    [_scrollView setMaximumZoomScale:1.0];
    [_scrollView setDelegate:self];
    [_scrollView addSubview:_imageView];
    
    [self startDownloadingImage];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}

- (void)showImage:(UIImage *)image {
    [_scrollView setZoomScale:1.0];
    
    [_imageView setImage:image];
//    [_imageView sizeToFit];
    [_imageView setFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    
    [_scrollView setContentSize:_imageView.bounds.size];
}

- (void)startLoadingIndicator {
    [_loadingIndicator startAnimating];
}

- (void)stopLoadingIndicator {
    [_loadingIndicator stopAnimating];
}

- (void)showErrorMessage {
    [self showAlertWithTitle:@"Error" message:@"Failed downloading image."];
}

- (void)previousImage {
    _currentIndex = (_currentIndex - 1);
    
    if (_currentIndex < 0) {
        _currentIndex = _imageURLStrings.count - 1;
    }
    
    [self startDownloadingImage];
}

- (void)nextImage {
    _currentIndex = (_currentIndex + 1) % _imageURLStrings.count;
    [self startDownloadingImage];
}

- (void)startDownloadingImage {
    [self startLoadingIndicator];
    [self showImage:nil];
    
    __weak ViewControllerH *weakSelf = self;
    
    id completionHandler = ^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf showImage:nil];
                [weakSelf stopLoadingIndicator];
                [weakSelf showErrorMessage];
            });
            return;
        }
        
        NSData *imageData = [NSData dataWithContentsOfURL:location];
        UIImage *image = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf showImage:image];
            [weakSelf stopLoadingIndicator];
        });
    };
    
    NSURL *imageURL = [NSURL URLWithString:_imageURLStrings[_currentIndex]];
    NSURLRequest *request = [NSURLRequest requestWithURL:imageURL];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request
                                                            completionHandler:completionHandler];
    
    [downloadTask resume];
}

@end
