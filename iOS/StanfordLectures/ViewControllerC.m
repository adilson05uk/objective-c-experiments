//
//  ViewControllerC.m
//  StanfordLectures
//
//  Created by Adil Hussain on 08/07/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerC.h"

@interface ViewControllerC ()

@property (weak, nonatomic) IBOutlet UILabel *labelAttributedString;
@property (weak, nonatomic) IBOutlet UITextView *textViewAttributedString;

@end

@implementation ViewControllerC

static NSString * const KEY_SOME_USER_DEFAULT = @"SOME_USER_DEFAULT";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller C"];
    
    [self initialiseLabelAttributedString];
}

- (IBAction)changeTextSelectionColorToRed:(id)sender {
    [self.textViewAttributedString.textStorage addAttribute:NSForegroundColorAttributeName
                                                      value:[UIColor redColor]
                                                      range:self.textViewAttributedString.selectedRange];
}

- (IBAction)changeTextSelectionColorToOrange:(id)sender {
    [self.textViewAttributedString.textStorage addAttribute:NSForegroundColorAttributeName
                                                      value:[UIColor orangeColor]
                                                      range:self.textViewAttributedString.selectedRange];
}

- (void)initialiseLabelAttributedString {
    NSString *name = @"Hulk Hogan";
    NSString *introString = [NSString stringWithFormat:@"Presenting the great... %@!", name];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:introString];
    
    NSRange range = [introString rangeOfString:name];
    
    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont systemFontOfSize:32.0]
                             range:range];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:[UIColor greenColor]
                             range:range];
    
    [attributedString addAttribute:NSStrokeWidthAttributeName
                             value:@-3
                             range:range];
    
    [attributedString addAttribute:NSStrokeColorAttributeName
                             value:[UIColor orangeColor]
                             range:range];
    
    [attributedString addAttribute:NSUnderlineStyleAttributeName
                             value:@(NSUnderlineStyleSingle)
                             range:range];
    
    self.labelAttributedString.attributedText = attributedString;
}

@end
