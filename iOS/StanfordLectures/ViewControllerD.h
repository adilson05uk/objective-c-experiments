//
//  ViewControllerD.h
//  StanfordLectures
//
//  Created by Adil Hussain on 22/08/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "BaseViewController.h"

@interface ViewControllerD : BaseViewController

@property (nonatomic, copy) NSString *text;

@end
