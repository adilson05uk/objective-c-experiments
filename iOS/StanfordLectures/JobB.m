//
//  JobB.m
//  StanfordLectures
//
//  Created by Adil Hussain on 07/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "JobB.h"

@implementation JobB

-(void)executeWithCompletionHandler:(void (^)(NSString *))completionHandler {
    NSLog(@"Executed JobB");
    completionHandler(@"Thanks for calling!");
}

@end
