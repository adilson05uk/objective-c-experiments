//
//  ViewControllerI.h
//  StanfordLectures
//
//  Created by Adil Hussain on 23/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "BaseViewController.h"

@interface ViewControllerI : BaseViewController <UITableViewDelegate, UITableViewDataSource>
@end
