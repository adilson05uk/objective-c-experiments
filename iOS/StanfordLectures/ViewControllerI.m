//
//  ViewControllerI.m
//  StanfordLectures
//
//  Created by Adil Hussain on 23/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerI.h"
#import "TableViewCellA.h"

@interface ViewControllerI ()
@end

@implementation ViewControllerI

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller I"];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Header %ld", section];
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Footer %ld", section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *reuseIdentifier = [TableViewCellA reuseIdentifier];
    NSString *nibName = [TableViewCellA nibName];
    
    TableViewCellA *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle bundleForClass:self.class] loadNibNamed:nibName owner:self options:nil];
        cell = [nib objectAtIndex:0];
    } else {
        NSLog(@"Reusing table view cell in section %ld, row %ld", indexPath.section, indexPath.row);
    }

    [cell configureViewsForRow:indexPath.row];
    [cell setAccessoryType:UITableViewCellAccessoryDetailButton];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSString *message = [NSString stringWithFormat:@"Tapped accessory button in section %ld and row %ld.", indexPath.section, indexPath.row];
    
    [self showAlertWithTitle:@"Accessory Button Tapped"
                     message:message];
}

@end
