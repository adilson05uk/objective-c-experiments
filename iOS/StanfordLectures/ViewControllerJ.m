//
//  PhotographersCDTVC.m
//  StanfordLectures
//
//  Created by Adil Hussain on 06/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "ViewControllerJ.h"
#import "ViewControllerK.h"
#import "Photographer+Create.h"
#import "Photo+CoreDataProperties.h"
#import "ManagedObjectContextProvider.h"

@interface ViewControllerJ ()
@end

@implementation ViewControllerJ {
    NSManagedObjectContext *_managedObjectContext;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller J"];
    
    UIBarButtonItem *addPhotographerButton = [[UIBarButtonItem alloc] initWithTitle:@"+Photographer"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(addPhotographer)];
    
    self.navigationItem.rightBarButtonItems = @[addPhotographerButton];
    
    _managedObjectContext = [ManagedObjectContextProvider provide];
    
    [self initialiseFetchedResultsController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    /*
     * need to reload the table view data because there are some data changes (i.e. update a photo) in ViewControllerK
     * (which this ViewController pushes) that don't cause an update to occur
     */
    [self.tableView reloadData];
}

- (void)initialiseFetchedResultsController {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photographer"];
    request.predicate = [NSPredicate predicateWithFormat:@"removed = %@", @NO];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:_managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"Photographer Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:simpleTableIdentifier];
    }
    
    Photographer *photographer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSUInteger numPhotos = 0;
    
    for (Photo *photo in photographer.photos) {
        if (!photo.removed) {
            numPhotos++;
        }
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = photographer.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu photos", numPhotos];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Photographer *photographer = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    ViewControllerK *viewController = [[ViewControllerK alloc] init];
    viewController.photographer = photographer;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Photographer *photographer = [self.fetchedResultsController objectAtIndexPath:indexPath];
        photographer.removed = YES;
    }
}

- (void)addPhotographer {
    NSUInteger count = self.fetchedResultsController.fetchedObjects.count;
    
    NSString *name = [NSString stringWithFormat:@"Some Photographer %lu", (count + 1)];
    
    NSDictionary *photographerData = @{PHOTOGRAPHER_NAME : name};
    
    [Photographer insertPhotographerWithData:photographerData
                    intoManagedObjectContext:_managedObjectContext];
}

@end
