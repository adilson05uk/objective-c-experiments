//
//  ViewControllerD.m
//  StanfordLectures
//
//  Created by Adil Hussain on 22/08/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerD.h"
#import "ViewA.h"
#import "ViewB.h"

@implementation ViewControllerD {
    __weak IBOutlet UITextView *textView;
    __weak IBOutlet ViewA *viewA;
    __weak IBOutlet ViewB *viewB;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.text) {
        textView.text = self.text;
    } else {
        textView.text = @"Did not receive any text from the presenter :-(";
    }
    
    viewA.fillColor = [UIColor greenColor];
    viewA.strokeColor = [UIColor blackColor];
    viewA.textColor = [UIColor redColor];
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    NSString *message;
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            message = @"Pan gesture began";
            break;
        case UIGestureRecognizerStateChanged:
            message = @"Pan gesture changed";
            break;
        case UIGestureRecognizerStateEnded:
            message = @"Pan gesture ended";
            break;
        case UIGestureRecognizerStateFailed:
            message = @"Pan gesture failed";
            break;
        case UIGestureRecognizerStateCancelled:
            message = @"Pan gesture cancelled";
            break;
        default:
            message = @"Unknown pan gesture";
            break;
    }
    
    textView.text = message;
}

- (IBAction)pinch:(UIPinchGestureRecognizer *)sender {
    NSString *message;
    
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            message = @"Pinch gesture began";
            break;
        case UIGestureRecognizerStateChanged:
            message = @"Pinch gesture changed";
            break;
        case UIGestureRecognizerStateEnded:
            message = @"Pinch gesture ended";
            break;
        case UIGestureRecognizerStateFailed:
            message = @"Pinch gesture failed";
            break;
        case UIGestureRecognizerStateCancelled:
            message = @"Pinch gesture cancelled";
            break;
        default:
            message = @"Unknown pinch gesture";
            break;
    }
    
    textView.text = message;
}

@end
