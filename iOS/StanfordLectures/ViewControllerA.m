//
//  ViewController.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerA.h"
#import "ViewControllerB.h"
#import "ViewControllerC.h"
#import "ViewControllerD.h"

@interface ViewControllerA ()

@property (assign, nonatomic) int someInt;
@property (weak, nonatomic) IBOutlet UILabel *someLabel;
@property (weak, nonatomic) IBOutlet UIImageView *someImageView;

@end

@implementation ViewControllerA {
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIView *contentView;
}

#pragma mark - UIViewController methods

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller A"];
    
    self.someInt = 0;
    [self refreshViews];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [scrollView layoutIfNeeded];
    scrollView.contentSize = contentView.bounds.size;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowViewControllerD"]) {
        if ([segue.destinationViewController isKindOfClass:[ViewControllerD class]]) {
            NSString *sourceViewControllerClassName = NSStringFromClass([self class]);
            NSString *message = [NSString stringWithFormat:@"This is the text passed in from %@.", sourceViewControllerClassName];
            
            ViewControllerD *viewControllerD = (ViewControllerD *)segue.destinationViewController;
            viewControllerD.text = message;
        }
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"ShowViewControllerD"]) {
        if (self.someInt % 2 == 0) {
            return YES;
        } else {
            self.someLabel.text = NSLocalizedString(@"CANNOT_PRESENT_VIEWCONTROLLERD", @"");
            
            return NO;
        }
    } else {
        return YES;
    }
}

- (IBAction)done:(UIStoryboardSegue *)segue {
    NSString *poppingBackMessageFormat = NSLocalizedString(@"POPPING_BACK_MESSAGE_FORMAT", @"");
    
    NSString *sourceViewControllerClassName = NSStringFromClass([segue.sourceViewController class]);
    NSString *message = [NSString stringWithFormat:poppingBackMessageFormat, sourceViewControllerClassName];
    
    self.someLabel.text = message;
}

#pragma mark - Xib actions

- (IBAction)someButtonAction:(id)sender {
    self.someInt++;
    [self refreshViews];
}

#pragma mark - ViewControllerA class-specific methods

- (void)refreshViews {
    self.someLabel.text = [NSString stringWithFormat:@"Count = %d", self.someInt];
    
    if (self.someInt % 2 == 0) {
        self.someImageView.image = [UIImage imageNamed:@"Clock"];
    } else {
        self.someImageView.image = [UIImage imageNamed:@"Compass"];
    }
}

@end
