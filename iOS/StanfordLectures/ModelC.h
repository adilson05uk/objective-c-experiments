//
//  ModelC.h
//  StanfordLectures
//
//  Created by Adil Hussain on 13/06/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelC : NSObject

@property (nonatomic, readonly) NSInteger propertyInteger;
@property (nonatomic, readonly) NSNumber *propertyNumber;

- (void)incrementPropertyInteger;
- (void)incrementPropertyNumber;

@end
