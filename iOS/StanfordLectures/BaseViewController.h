//
//  BaseViewController.h
//  StanfordLectures
//
//  Created by Adil Hussain on 08/07/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
