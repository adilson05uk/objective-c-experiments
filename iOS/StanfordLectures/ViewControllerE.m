//
//  ViewControllerE.m
//  StanfordLectures
//
//  Created by Adil Hussain on 07/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerE.h"
#import "JobA.h"
#import "JobB.h"

@interface ViewControllerE ()
@end

@implementation ViewControllerE {
    JobA *_jobA;
    JobB *_jobB;
    __weak IBOutlet UIButton *_animateViewButton;
    __weak IBOutlet UIButton *_transitionViewButton;
    __weak IBOutlet UIView *_view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _jobA = [[JobA alloc] init];
    _jobB = [[JobB alloc] init];
}

- (IBAction)executeJobA:(id)sender {
    __weak ViewControllerE *weakSelf = self;
    
    void (^completionHandler)(void) = ^{
        [weakSelf showAlertWithTitle:@"Completion Handler" message:@"Received callback from JobA"];
    };
    
    [_jobA executeWithCompletionHandler:completionHandler];
}

- (IBAction)executeJobB:(id)sender {
    __weak ViewControllerE *weakSelf = self;
    
    void (^completionHandler)(NSString *) = ^(NSString *input){
        NSString *title = @"Completion Handler";
        NSString *message = [NSString stringWithFormat:@"Received message from JobB: %@", input];
        
        [weakSelf showAlertWithTitle:title message:message];
    };
    
    [_jobB executeWithCompletionHandler:completionHandler];
}

- (IBAction)animateView:(id)sender {
    _animateViewButton.enabled = NO;
    
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut;
    
    [UIView animateWithDuration:3.0
                          delay:0.0
                        options:options
                     animations:^{
                         self->_view.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self->_animateViewButton.enabled = YES;
                         self->_view.alpha = 1.0;
                     }
     ];
}

- (IBAction)transitionView:(id)sender {
    _transitionViewButton.enabled = NO;
    
    UIViewAnimationOptions options = UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionCurveEaseInOut;
    
    [UIView transitionWithView:_view
                      duration:3.0
                       options:options
                    animations:^{
                        // nothing to do
                    }
                    completion:^(BOOL finished){
                        self->_transitionViewButton.enabled = YES;
                    }
     ];
}

@end
