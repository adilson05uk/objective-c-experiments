//
//  ViewControllerF.h
//  StanfordLectures
//
//  Created by Adil Hussain on 11/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "BaseViewController.h"

@interface ViewControllerF : BaseViewController <UITableViewDelegate, UITableViewDataSource>
@end
