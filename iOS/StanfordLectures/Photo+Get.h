//
//  Photo+Get.h
//  StanfordLectures
//
//  Created by Adil Hussain on 20/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Photo+CoreDataProperties.h"

@interface Photo (Get)

+ (Photo *)getPhotoWithObjectID:(NSManagedObjectID *)objectID
       fromManagedObjectContext:(NSManagedObjectContext *)context;

@end
