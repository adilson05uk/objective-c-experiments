//
//  SomeModel.h
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProtocolA.h"

@interface ModelA : NSObject <ProtocolA>

@property (strong, nonatomic, setter=setPropertyStringAlternative:) NSString *propertyString;
@property (nonatomic, readonly, getter=isPropertyBool) BOOL propertyBool;

@end
