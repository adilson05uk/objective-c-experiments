//
//  AppDelegate.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

@end
