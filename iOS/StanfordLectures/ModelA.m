//
//  SomeModel.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ModelA.h"

@implementation ModelA

-(void)setPropertyStringAlternative:(NSString *)propertyString {
    _propertyString = propertyString;
    _propertyBool = propertyString != nil;
}

-(NSString *)description {
    NSString *className = NSStringFromClass([self class]);
    return [NSString stringWithFormat:@"%@ - Model class that has a property with a getter and setter override.", className];
}

#pragma mark - ProtocolA methods

-(int)matchAgainstSingleItem:(NSString *)item {
    if ([self.propertyString isEqualToString:item]) {
        return 1;
    } else {
        return 0;
    }
}

-(int)matchAgainstMultipleItems:(NSArray *)items {
    int count = 0;
    
    for (NSString *item in items) {
        count += [self matchAgainstSingleItem:item];
    }
    
    return count;
}

@end
