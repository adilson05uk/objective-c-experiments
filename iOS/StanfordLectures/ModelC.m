//
//  ModelC.m
//  StanfordLectures
//
//  Created by Adil Hussain on 13/06/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ModelC.h"

@interface ModelC()

@property (nonatomic, readwrite) NSInteger propertyInteger;
@property (nonatomic, readwrite) NSNumber *propertyNumber;

@end

@implementation ModelC

- (NSNumber *)propertyNumber {
    if (_propertyNumber == nil) {
        _propertyNumber = @0;
    }
    
    return _propertyNumber;
}

- (void)incrementPropertyInteger {
    self.propertyInteger++;
}

- (void)incrementPropertyNumber {
    self.propertyNumber = @(self.propertyNumber.longValue + 1);
}

-(NSString *)description {
    NSString *className = NSStringFromClass([self class]);
    return [NSString stringWithFormat:@"%@ - Model class that has a property that is *readonly* publicy and *readwrite* privately.", className];
}

@end
