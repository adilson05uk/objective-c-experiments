//
//  JobB.h
//  StanfordLectures
//
//  Created by Adil Hussain on 07/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobB : NSObject

-(void)executeWithCompletionHandler:(void (^)(NSString *))completionHandler;

@end
