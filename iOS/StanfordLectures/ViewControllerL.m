//
//  ViewControllerL.m
//  StanfordLectures
//
//  Created by Adil Hussain on 17/04/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "ViewControllerL.h"
#import <CoreMotion/CoreMotion.h>

@interface ViewControllerL ()

@property(nonatomic, weak) UILabel *pauseLabel;

@property(nonatomic, weak) UIView *redBlock;
@property(nonatomic, weak) UIView *blackBlock;

@property(nonatomic, strong) UIDynamicAnimator *animator;
@property(nonatomic, weak) UIGravityBehavior *gravity;
@property(nonatomic, weak) UICollisionBehavior *collider;
@property(nonatomic, weak) UIDynamicItemBehavior *elastic;
@property(nonatomic, weak) UIDynamicItemBehavior *quicksand;

@property(nonatomic, strong) CMMotionManager *motionManager;

@end

@implementation ViewControllerL

static CGSize blockSize = {40, 40};

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"View Controller L"];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(tap)];

    [self.view addGestureRecognizer:tapGestureRecognizer];

    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [self pauseGame];
                                                  }];

    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      // only resume if we regain active and this VC is on screen at the time
                                                      if (self.view.window) [self resumeGame];
                                                  }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self resumeGame];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self pauseGame];
}

- (void)resumeGame {
    if (!self.redBlock) {
        self.redBlock = [self addBlockOffsetFromCenterBy:UIOffsetMake(-100, 0)];
        self.redBlock.backgroundColor = [UIColor redColor];

        [self.collider addItem:self.redBlock];
        [self.elastic addItem:self.redBlock];
        [self.gravity addItem:self.redBlock];
        [self.quicksand addItem:self.redBlock];
    }

    if (!self.blackBlock) {
        self.blackBlock = [self addBlockOffsetFromCenterBy:UIOffsetMake(+100, 0)];
        self.blackBlock.backgroundColor = [UIColor blackColor];

        [self.collider addItem:self.blackBlock];
        [self.quicksand addItem:self.blackBlock];
    }

    self.pauseLabel.text = @"";
    
    self.quicksand.resistance = 0;
    self.gravity.gravityDirection = CGVectorMake(0, 0);

    if (!self.motionManager.isAccelerometerActive) {
        [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue]
                                                 withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                                                     CGFloat x = accelerometerData.acceleration.x;
                                                     CGFloat y = accelerometerData.acceleration.y;
                                                     switch ([[UIApplication sharedApplication] statusBarOrientation]) {
                                                         case UIInterfaceOrientationLandscapeRight:
                                                             self.gravity.gravityDirection = CGVectorMake(-y, -x);
                                                             break;
                                                         case UIInterfaceOrientationLandscapeLeft:
                                                             self.gravity.gravityDirection = CGVectorMake(y, x);
                                                             break;
                                                         case UIInterfaceOrientationPortrait:
                                                             self.gravity.gravityDirection = CGVectorMake(x, -y);
                                                             break;
                                                         case UIInterfaceOrientationPortraitUpsideDown:
                                                             self.gravity.gravityDirection = CGVectorMake(-x, y);
                                                             break;
                                                         default:
                                                             // nothing to do
                                                             break;
                                                     }
                                                 }];
    }
}

- (void)pauseGame {
    self.pauseLabel.text = @"Paused";

    self.quicksand.resistance = 10.0;
    self.gravity.gravityDirection = CGVectorMake(0, 0);

    [self.motionManager stopAccelerometerUpdates];
}

- (BOOL)isPaused {
    return !self.motionManager.isAccelerometerActive;
}

#pragma mark - IBAction

- (IBAction)tap {
    if ([self isPaused]) {
        [self resumeGame];
    } else {
        [self pauseGame];
    }
}

#pragma mark - UIView

- (UIView *)addBlockOffsetFromCenterBy:(UIOffset)offset {
    CGPoint blockCenter = CGPointMake(
            CGRectGetMidX(self.view.bounds) + offset.horizontal,
            CGRectGetMidY(self.view.bounds) + offset.vertical
    );

    CGRect blockFrame = CGRectMake(
            blockCenter.x - blockSize.width / 2,
            blockCenter.y - blockSize.height / 2,
            blockSize.width,
            blockSize.height
    );

    UIView *block = [[UIView alloc] initWithFrame:blockFrame];

    [self.view addSubview:block];

    return block;
}

- (UILabel *)pauseLabel {
    if (!_pauseLabel) {
        UILabel *pauseLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
        pauseLabel.font = [pauseLabel.font fontWithSize:64];
        pauseLabel.textAlignment = NSTextAlignmentCenter;
        pauseLabel.numberOfLines = 1;
        pauseLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin |
                UIViewAutoresizingFlexibleTopMargin |
                UIViewAutoresizingFlexibleLeftMargin |
                UIViewAutoresizingFlexibleRightMargin;

        [self.view addSubview:pauseLabel];

        _pauseLabel = pauseLabel;
    }
    return _pauseLabel;
}

#pragma mark - UIDynamicAnimator and Behaviors

- (UIDynamicAnimator *)animator {
    if (!_animator) _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    return _animator;
}

- (UICollisionBehavior *)collider {
    if (!_collider) {
        UICollisionBehavior *collider = [[UICollisionBehavior alloc] init];
        collider.translatesReferenceBoundsIntoBoundary = YES;
        [self.animator addBehavior:collider];
        _collider = collider;
    }
    return _collider;
}

- (UIGravityBehavior *)gravity {
    if (!_gravity) {
        UIGravityBehavior *gravity = [[UIGravityBehavior alloc] init];
        [self.animator addBehavior:gravity];
        _gravity = gravity;
    }
    return _gravity;
}

- (UIDynamicItemBehavior *)elastic {
    if (!_elastic) {
        UIDynamicItemBehavior *elastic = [[UIDynamicItemBehavior alloc] init];
        elastic.elasticity = 1.0;
        [self.animator addBehavior:elastic];
        _elastic = elastic;
    }
    return _elastic;
}

- (UIDynamicItemBehavior *)quicksand {
    if (!_quicksand) {
        UIDynamicItemBehavior *quicksand = [[UIDynamicItemBehavior alloc] init];
        quicksand.resistance = 0;
        [self.animator addBehavior:quicksand];
        _quicksand = quicksand;
    }
    return _quicksand;
}

#pragma mark - Core Motion

- (CMMotionManager *)motionManager {
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
        _motionManager.accelerometerUpdateInterval = 0.1;
    }
    return _motionManager;
}

@end
