//
//  TableViewCellA.m
//  StanfordLectures
//
//  Created by Adil Hussain on 25/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "TableViewCellA.h"

@implementation TableViewCellA {
    
    __weak IBOutlet UILabel *_title;
    __weak IBOutlet UILabel *_detail;
}

+ (NSString *)reuseIdentifier {
    return @"TableViewCellA";
}

+ (NSString *)nibName {
    return @"TableViewCellA";
}

- (void)configureViewsForRow:(NSInteger)row {
    NSString *titleText = [NSString stringWithFormat:@"Title %ld", row];
    NSString *detailText = [NSString stringWithFormat:@"Detail %ld", row];
    
    _title.text = titleText;
    _detail.text = detailText;
}

@end
