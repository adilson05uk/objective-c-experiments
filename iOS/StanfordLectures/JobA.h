//
//  JobA.h
//  StanfordLectures
//
//  Created by Adil Hussain on 07/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JobA : NSObject

-(void)executeWithCompletionHandler:(void (^)(void))completionHandler;

@end
