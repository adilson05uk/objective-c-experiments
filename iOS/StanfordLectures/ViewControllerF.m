//
//  ViewControllerF.m
//  StanfordLectures
//
//  Created by Adil Hussain on 11/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "ViewControllerF.h"

typedef NS_ENUM(NSInteger, ViewControllerId) {
    ViewControllerIdA = 0,
    ViewControllerIdB,
    ViewControllerIdC,
    ViewControllerIdD,
    ViewControllerIdE,
    ViewControllerIdF,
    ViewControllerIdG,
    ViewControllerIdH,
    ViewControllerIdI,
    ViewControllerIdJ,
    ViewControllerIdL,
    ViewControllerIdsSize
};

@interface ViewControllerF ()
@end

@implementation ViewControllerF

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ViewControllerIdsSize;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:simpleTableIdentifier];
    }
    
    ViewControllerId viewControllerId = indexPath.row;
    
    cell.textLabel.text = [self getViewControllerNameFromId:viewControllerId];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ViewControllerId viewControllerId = indexPath.row;
    NSString *viewControllerName = [self getViewControllerNameFromId:viewControllerId];
    UIViewController *viewController;
    
    switch (viewControllerId) {
        case ViewControllerIdB:
        case ViewControllerIdC:
        case ViewControllerIdG:
        case ViewControllerIdH:
        case ViewControllerIdI:
        case ViewControllerIdJ:
        case ViewControllerIdL:
            viewController = [[NSClassFromString(viewControllerName) alloc] init];
            break;
            
        default:
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:viewControllerName];
            break;
    }
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (NSString *)getViewControllerNameFromId:(ViewControllerId)viewControllerId {
    switch (viewControllerId) {
        case ViewControllerIdA:
            return @"ViewControllerA";
        case ViewControllerIdB:
            return @"ViewControllerB";
        case ViewControllerIdC:
            return @"ViewControllerC";
        case ViewControllerIdD:
            return @"ViewControllerD";
        case ViewControllerIdE:
            return @"ViewControllerE";
        case ViewControllerIdF:
            return @"ViewControllerF";
        case ViewControllerIdG:
            return @"ViewControllerG";
        case ViewControllerIdH:
            return @"ViewControllerH";
        case ViewControllerIdI:
            return @"ViewControllerI";
        case ViewControllerIdJ:
            return @"ViewControllerJ";
        case ViewControllerIdL:
            return @"ViewControllerL";
        default:
            return nil;
    }
}

@end
