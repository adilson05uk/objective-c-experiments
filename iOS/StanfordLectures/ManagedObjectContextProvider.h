//
//  PhotomaniaAppDelegate+MOC.h
//  Photomania
//
//  This code comes from the Xcode template for Master-Detail application.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ManagedObjectContextProvider : NSObject

+ (NSManagedObjectContext *)provide;

@end
