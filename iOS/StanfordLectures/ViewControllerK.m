//
//  ViewControllerK.m
//  StanfordLectures
//
//  Created by Adil Hussain on 13/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import "ViewControllerK.h"
#import "Photo+Create.h"
#import "ManagedObjectContextProvider.h"

@interface ViewControllerK ()
@end

@implementation ViewControllerK

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:self.photographer.name];
    
    UIBarButtonItem *addPhotoButton = [[UIBarButtonItem alloc] initWithTitle:@"+Photo"
                                                                       style:UIBarButtonItemStylePlain
                                                                      target:self
                                                                      action:@selector(addPhoto)];
    
    self.navigationItem.rightBarButtonItems = @[addPhotoButton];
    
    [self initialiseFetchedResultsController];
}

- (void)initialiseFetchedResultsController {
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"whoTook = %@", self.photographer];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"removed = %@", @NO];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.predicate = predicate;
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                        managedObjectContext:self.photographer.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"Photo Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:simpleTableIdentifier];
    }
    
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.subtitle;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
        photo.removed = YES;
    }
}

- (void)addPhoto {
    NSUInteger count = self.fetchedResultsController.fetchedObjects.count;
    
    NSString *title = [NSString stringWithFormat:@"Some Title %lu", (count + 1)];
    NSString *description = [NSString stringWithFormat:@"Some Description %lu", (count + 1)];
    
    NSDictionary *photoData = @{PHOTO_TITLE : title,
                                PHOTO_DESCRIPTION : description};
    
    [Photo insertPhotoWithData:photoData
                  photographer:self.photographer
      intoManagedObjectContext:self.photographer.managedObjectContext];
}

@end
