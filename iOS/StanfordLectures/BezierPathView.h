//
//  BezierPathView.h
//  StanfordLectures
//
//  Created by Adil Hussain on 04/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BezierPathView : UIView

@property (strong, nonatomic) UIBezierPath *path;

@end
