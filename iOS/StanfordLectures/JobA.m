//
//  JobA.m
//  StanfordLectures
//
//  Created by Adil Hussain on 07/10/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import "JobA.h"

@implementation JobA

-(void)executeWithCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"Executed JobA");
    completionHandler();
}

@end
