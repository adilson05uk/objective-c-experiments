//
//  StanfordLecturesUITests.m
//  StanfordLecturesUITests
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface StanfordLecturesUITests : XCTestCase
@end

@implementation StanfordLecturesUITests {
    XCUIApplication *_application;
}

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    
    _application = [[XCUIApplication alloc] init];
    [_application launch];
}

- (void)test_view_state_when_view_controller_is_launched {
    XCTAssertTrue(_application.staticTexts[@"Count = 0"].exists);
}

- (void)test_view_state_when_increment_count_button_is_tapped {
    // When.
    [_application.buttons[@"Increment Count"] tap];
    
    // Then.
    XCTAssertTrue(_application.staticTexts[@"Count = 1"].exists);
}

@end
