//
//  PhotoTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 20/01/2017.
//  Copyright © 2017 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Photo+Create.h"
#import "Photo+Get.h"
#import "Photographer+Create.h"
#import "ManagedObjectContextProvider.h"

@interface PhotoTest : XCTestCase
@end

@implementation PhotoTest {
    NSManagedObjectContext *_managedObjectContext;
}

- (void)setUp {
    [super setUp];
    _managedObjectContext = [ManagedObjectContextProvider provide];
}

- (void)test_get_after_put {
    // Given.
    NSDictionary *photographerData = @{PHOTOGRAPHER_NAME : @"Some Name"};
    
    Photographer *photographer = [Photographer insertPhotographerWithData:photographerData
                                                 intoManagedObjectContext:_managedObjectContext];
    
    NSDictionary *photoData = @{PHOTO_TITLE : @"Some Title",
                                PHOTO_DESCRIPTION : @"Some Description"};
    
    Photo *photoInserted = [Photo insertPhotoWithData:photoData
                                         photographer:photographer
                             intoManagedObjectContext:_managedObjectContext];
    
    // When.
    Photo *photoRetrieved = [Photo getPhotoWithObjectID:photoInserted.objectID
                               fromManagedObjectContext:_managedObjectContext];
    
    // Then.
    XCTAssertEqualObjects(photoData[PHOTO_TITLE], photoRetrieved.title);
    XCTAssertEqualObjects(photoData[PHOTO_DESCRIPTION], photoRetrieved.subtitle);
    XCTAssertEqualObjects(photographerData[PHOTOGRAPHER_NAME], photoRetrieved.whoTook.name);
}

- (void)test_get_after_delete {
    // Given.
    NSDictionary *photographerData = @{PHOTOGRAPHER_NAME : @"Some Name"};
    
    Photographer *photographer = [Photographer insertPhotographerWithData:photographerData
                                                 intoManagedObjectContext:_managedObjectContext];
    
    NSDictionary *photoData = @{PHOTO_TITLE : @"Some Title",
                                PHOTO_DESCRIPTION : @"Some Description"};
    
    Photo *photoInserted = [Photo insertPhotoWithData:photoData
                                         photographer:photographer
                             intoManagedObjectContext:_managedObjectContext];
    
    [_managedObjectContext deleteObject:photoInserted];
    
    // When.
    Photo *photoRetrieved = [Photo getPhotoWithObjectID:photoInserted.objectID
                               fromManagedObjectContext:_managedObjectContext];
    
    // Then.
    XCTAssertNil(photoRetrieved);
}

@end
