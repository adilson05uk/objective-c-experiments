//
//  ModelCTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 13/06/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ModelC.h"

@interface ModelCTest : XCTestCase
@end

@implementation ModelCTest {
    ModelC *target;
}

- (void)setUp {
    [super setUp];
    target = [[ModelC alloc] init];
}

- (void)test_getPropertyInteger {
    // When. / Then.
    XCTAssertEqual(target.propertyInteger, 0);
}

- (void)test_incrementPropertyInteger {
    // When.
    [target incrementPropertyInteger];
    
    // Then.
    XCTAssertEqual(target.propertyInteger, 1);
}

- (void)test_getPropertyNumber {
    // When. / Then.
    XCTAssertEqualObjects(target.propertyNumber, @0);
}

- (void)test_incrementPropertyNumber {
    // When.
    [target incrementPropertyNumber];
    
    // Then.
    XCTAssertEqualObjects(target.propertyNumber, @1);
}

@end
