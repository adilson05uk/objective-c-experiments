//
//  NSUserDefaultsTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 12/07/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface NSUserDefaultsTest : XCTestCase
@end

@implementation NSUserDefaultsTest

- (void)test_save_and_retrieve_string {
    // Given.
    NSString *key = @"SomeKey";
    
    NSString *inputString = @"SomeValue";
    
    [[NSUserDefaults standardUserDefaults] setObject:inputString forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // When.
    NSString *outputString = [[NSUserDefaults standardUserDefaults] stringForKey:key];
    
    // Then.
    XCTAssertEqualObjects(outputString, inputString);
}

- (void)test_save_and_retrieve_array {
    // Given.
    NSString *key = @"SomeKey";
    
    NSArray *inputArray = @[ @"Value0", @"Value1" ];
    
    [[NSUserDefaults standardUserDefaults] setObject:inputArray forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // When.
    NSArray *outputArray = [[NSUserDefaults standardUserDefaults] arrayForKey:key];
    
    // Then.
    XCTAssertEqualObjects(outputArray, inputArray);
}

- (void)test_save_and_retrieve_dictionary {
    // Given.
    NSString *key = @"SomeKey";
    
    NSArray *inputArray = @[ @"Value0", @"Value1" ];
    NSDictionary *inputDictionary = @{ @"SomeKey" : inputArray };
    
    [[NSUserDefaults standardUserDefaults] setObject:inputDictionary forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // When.
    NSDictionary *outputDictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:key];

    // Then.
    XCTAssertEqualObjects(outputDictionary, inputDictionary);
}

@end
