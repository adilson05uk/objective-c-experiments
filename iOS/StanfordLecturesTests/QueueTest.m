//
//  QueueTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 11/11/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface QueueTest : XCTestCase
@end

@implementation QueueTest {
    XCTestExpectation *expectation;
}

- (void)test_dispatch_async {
//    dispatch_queue_t mainQ1 = dispatch_get_main_queue();
//    NSOperationQueue *mainQ2 = [NSOperationQueue mainQueue];
    dispatch_queue_t someQ = dispatch_queue_create("someQ", NULL);
    
    expectation = [self expectationWithDescription:@"Dispatching a block to be executed on a queue."];
    
    dispatch_async(someQ, ^{
        [self logMessage:@"Queue"];
    });
    
    [self waitForExpectationsWithTimeout:3 handler:^(NSError *error) {
        // do test clearup
    }];
    
    XCTAssertTrue(true);
}

- (void)test_performSelector {
    expectation = [self expectationWithDescription:@"Performing selector on main thread."];
    
    [self performSelectorOnMainThread:@selector(logMessage:)
                           withObject:@"Selector"
                        waitUntilDone:NO];
    
    [self waitForExpectationsWithTimeout:3 handler:^(NSError *error) {
        // do test clearup
    }];
    
    XCTAssertTrue(true);
}

- (void)logMessage:(NSString *)message {
    NSLog(@"Hello %@!", message);
    [NSThread sleepForTimeInterval:1];
    NSLog(@"Goodbye %@!", message);
    
    [expectation fulfill];
}

@end
