//
//  BlockTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 28/09/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface BlockTest : XCTestCase
@end

@implementation BlockTest

- (void)test_enumerateKeysAndObjectsUsingBlock {
    NSLog(@"Here I am!!");
    
    __block BOOL stoppedEarly = NO;
    
    NSString *stopKey = @"KeyB";
    NSString *stopValue = @"ValueD";
    
    id block = ^(id key, id value, BOOL *stop) {
        NSLog(@"value for key %@ is %@", key, value);
        
        if ([stopKey isEqualToString:key]) {
            NSLog(@"Encountered the stop key!");
            *stop = YES;
        }
        
        if ([stopValue isEqualToString:value]) {
            NSLog(@"Encountered the stop value!");
            *stop = YES;
            stoppedEarly = YES;
        }
    };
    
    NSDictionary *dictionary = @{ @"KeyA" : @"ValueA",
                                  @"KeyB" : @"ValueB",
                                  @"KeyC" : @"ValueC",
                                  @"KeyD" : @"ValueD",
                                  @"KeyE" : @"ValueE" };
    
    [dictionary enumerateKeysAndObjectsUsingBlock:block];
    
    NSLog(@"Did the enumeration stop early? %d", stoppedEarly);
}

- (void)test_retrievingAndExecutingBlocksInNSArray {
    id block = ^{
        NSLog(@"Block executing...");
    };
    
    NSArray *blocks = @[ block, block, block ];
    
    for (int i = 0; i < blocks.count; i++) {
        void (^executeBlock)(void) = blocks[i];
        executeBlock();
    }
    
    for (id blockIth in blocks) {
        void (^executeBlock)(void) = blockIth;
        executeBlock();
    }
}

- (void)test_executingBlockWithParameter {
    void (^block)(NSString *inputA) = ^(NSString *inputA) {
        NSLog(@"Block received input: %@", inputA);
    };
    
    block(@"Hello");
}

- (void)test_executingBlockWithNonVoidReturnType {
    NSString *(^block)(void) = ^() {
        return @"Hello";
    };
    
    NSString *output = block();
    
    NSLog(@"Received output from block: %@", output);
}

@end
