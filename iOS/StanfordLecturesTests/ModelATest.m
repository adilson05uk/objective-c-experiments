//
//  StanfordLecturesTests.m
//  StanfordLecturesTests
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ModelA.h"

@interface ModelATest : XCTestCase
@end

@implementation ModelATest {
    ModelA *target;
}

-(void)setUp {
    [super setUp];
    target = [ModelA new];
}

-(void)test_setPropertyString_withNilString {
    // When.
    target.propertyString = nil;
    
    // Then.
    XCTAssertNil(target.propertyString);
    XCTAssertFalse(target.propertyBool);
    XCTAssertFalse(target.isPropertyBool);
}

-(void)test_setPropertyString_withNonNilString {
    // When.
    target.propertyString = @"Some value";
    
    // Then.
    XCTAssertEqualObjects(target.propertyString, @"Some value");
    XCTAssertTrue(target.propertyBool);
    XCTAssertTrue(target.isPropertyBool);
}

-(void)test_matchAgainstSingleItem_withBadMatch {
    // Given.
    target.propertyString = @"Some value";
    
    // When.
    int result = [target matchAgainstSingleItem:@"Some other value"];
    
    // Then.
    XCTAssertEqual(result, 0);
}

-(void)test_matchAgainstSingleItem_withGoodMatch {
    // Given.
    target.propertyString = @"Some value";
    
    // When.
    int result = [target matchAgainstSingleItem:@"Some value"];
    
    // Then.
    XCTAssertEqual(result, 1);
}

-(void)test_matchAgainstMultipleItems_withMultipleGoodMatches {
    // Given.
    target.propertyString = @"A";
    
    NSArray *items = @[@"A",@"B",@"C",@"A",@"B",@"C"];
    
    // When.
    int result = [target matchAgainstMultipleItems:items];
    
    // Then.
    XCTAssertEqual(result, 2);
}

@end
