//
//  ModelBTest.m
//  StanfordLectures
//
//  Created by Adil Hussain on 26/05/2016.
//  Copyright © 2016 Think In Code. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ModelB.h"

@interface ModelBTest : XCTestCase
@end

@implementation ModelBTest {
    ModelB *target;
}

- (void)setUp {
    [super setUp];
    target = [ModelB new];
}

- (void)testAddAndRemove {
    // When.
    [target addItem:@"A"];
    [target addItem:@"B"];
    
    NSString *firstPoppedItem = [target removeItem];
    NSString *secondPoppedItem = [target removeItem];
    NSString *thirdPoppedItem = [target removeItem];

    // Then.
    XCTAssertEqualObjects(firstPoppedItem, @"B");
    XCTAssertEqualObjects(secondPoppedItem, @"A");
    XCTAssertNil(thirdPoppedItem);
}

@end
